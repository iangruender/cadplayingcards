// Playing Card
// Chris Dorn

#include <iostream>
#include <conio.h>

using namespace std;

enum Suit { Hearts, Diamonds, Clubs, Spades};

enum Rank { Two = 2,Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace};

struct Card
{
	Suit Suit;
	Rank Rank;
};

void PrintCard(Card card) 
{
	cout << "The ";
	switch (card.Rank)
	{
	case Two:
		cout << "Two of ";
		break;
	case Three:
		cout << "Three of ";
		break;
	case Four:
		cout << "Four of ";
		break;
	case Five:
		cout << "Five of ";
		break;
	case Six:
		cout << "Six of ";
		break;
	case Seven:
		cout << "Seven of ";
		break;
	case Eight:
		cout << "Eight of ";
		break;
	case Nine:
		cout << "Nine of ";
		break;
	case Ten:
		cout << "Ten of ";
		break;
	case Jack:
		cout << "Jack of ";
		break;
	case Queen:
		cout << "Queen of ";
		break;
	case King:
		cout << "King of ";
		break;
	case Ace:
		cout << "Ace of ";
		break;
	default:
		break;
	}
	switch (card.Suit)
	{
	case Hearts:
		cout << "Hearts";
		break;
	case Diamonds:
		cout << "Diamonds";
		break;
	case Clubs:
		cout << "Clubs";
		break;
	case Spades:
		cout << "Spades";
		break;
	default:
		break;
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.Rank >= card2.Rank) return card1;
	if (card2.Rank > card1.Rank) return card2;
}

int main()
{
	Card card1;
	card1.Rank = King;
	card1.Suit = Hearts;
	PrintCard(card1);

	(void)_getch();
	return 0;
}